---
geometry:
- top=0.75in
- bottom=1in
- left=0.5in
- right=1in
mainfont: Roboto
documentclass: extarticle
fontsize: 10pt
colorlinks: true
linkcolor: Blue
urlcolor: Blue
---

<p style="text-align: center; font-size: 36px; margin-bottom: 1px;">
Stan 
<span style="font-weight: bold">Bogatkin</span>
</p>
<p style="text-align: center; font-size: 14px">Principal DevOps Engineer</p>

---

## Short summary

#### DevOps engineer with more than 15 years of systems delivery experience, which includes

* Experience with deployment and migration in heterogeneous environments
* Development key application tools for system deployment and life cycle management
* Plan and delivery of solid cloud solutions for 500+ nodes
* Lead the effort in infrastructure scaling, availability, reliability and
  efficiency via software and automation
* Debug complex issues found in production between multiple systems or services
* Act independently to establish repeatable methods and procedures to reduce or
  eliminate blockers and dependencies. Learning to resolve blockers and
  dependencies for the team
* Work to understand critical path items and create health check metrics and
  dashboards

#### Skills and Awards:

* _Network core:_ proficient knowledge of TCP/IP stack
* _Security core:_ Firewall, IPSEC/VPN,  SSL/TLS, RB/Mantatory access contols
* _Cloud proficient:_ OpenStack (most of the key services up to current versions), AWS, common broker messaging – RabbitMQ, Oslo messaging, Google App Engine (BigTable, Memcache API, Firebase), MS Azure, Docker (with Compose and Swarm)
* _Monitoring and logging:_ Victoriametrics, Prometheus, Sensu, rsyslog/syslog-ng, Grafana, ElasticSearch/Logstash/Kibana/Graylog, Nagios, CollectD, 
* _Time-Series Dbs:_ VictoriaMetrics, Graphite (whisper), Prometheus Carbon, TimescaleDB
* _Clouds experience:_ OpenStack, Azure, AWS
* _Container-related tools:_ Docker, Podman, Kubernetes
* _Linux core expertise:_ network stack, FS, kernel tracing, automating, monitoring etc.
* _Provisioning and deploy:_ Ansible, Puppet, MaaS, Fuel
* _IaaC tooling:_ Terraform, Pulumi
* _Development:_
    * Python 2/3 and frameworks (Flask, SQLAlchemy etc.)
    * Java Core, Android-related Java frameworks and libraries (Dagger, Rx, ButterKnife, GSON/Jackson, Okio/OkHttp, Retrofit)
    * Ruby DSLs
    * Shell scripting
* _Software Design:_ Design patterns, Architectural pattens (CLEAN, VIPER, MVP etc.);
* _Software Engineering Management:_ JIRA, Trello, Slack, Confluence, Redmine
* _CI/CD and tools:_ Jenkins, Gitlab, TravisCI, Github Actions, CircleCI,  BitBucket, plain Git
* _Spoken Languages:_ English, Russian;

---

## Work experience
### **Tinkoff bank**, <https://tinkoff.ru>
>### Oct 2022 - Current: Public cloud lead
>>_Customer:_  Internal company services\
>>_Team Size:_ 1-10\
>>_Project Role:_ Project Lead\
>>_Employment type:_ Full-time\
>>_Daily routine:_
>>
>>* Manage public cloud company leadership
>### Mar 2021 - Oct 2022: Solution Architect
>>_Customer:_  Internal company services\
>>_Team Size:_ 1-10\
>>_Project Role:_ Solution Architect\
>>_Employment type:_ Full-time\
>>_Daily routine:_
>>
>>* Improve processes in public cloud area


### **CoderShip**, <https://galeracluster.com>
>### Nov 2019 - Feb 2022: DevOps
>_Customer:_  Internal company services\
>_Team Size:_ 1-10\
>_Project Role:_ Infrastructure Engineer, QA >Engineer\
>_Employment type:_ Part-time\
>_Daily routine:_
>
>* Migrate from old deployment system to automated new one
>* Ensure automation for testing, packaging and deployment of upstream MySQL/Galera
>* Support and develop existing CI/CD system
>* Implement new infrastructure features


### **360tv**, <https://360tv.ru/>
>### June 2017 - Current: SRE
>_Customer:_  Internal company services\
>_Team Size:_ 1-10\
>_Project Role:_ SRE\
>_Employment type:_ Part-time\
>_Daily routine:_
>
>* Constantly improve existing company infrastructure tooling
>* Help dev team to implement better solutions
>
>_Tasks Performed:_
>
>* Created CI/CD system based on Jenkins
>* Created automated monitoring and support of internal infrastructure services
>* Automated infrastructure changes deployment based >on Ansible stack
>* Created backup system for tens of DBs and more than 500Gb files
>* Maintain most popular TV website of Moscow Region
>* Implemented internal solution to work with git repositories
>* Implemented and maintain infrastructure for more than 10 external projects
>* Implemented set of internal storages - Git, NPM, Docker registry

### **Oom.ag**, <https://oom.ag/>
>### March 2021 - Current: Solution Architect
>>_Customer:_  Internal company services\
>>_Team Size:_ 30-50\
>>_Project Role:_ Solution Architect\
>>_Employment type:_ Part-time\
>>_Daily routine:_
>>
>>* Implement new solutions which meets company needs
>>* Lead Ops team
>>
>>_Tasks Performed:_
>>
>>* Moved company projects from Github/Travis CI stack to GitLab stack
>>* Implement private docker registry with images security audit
>>* Moved from Ansible-based deployments to Docker-based ones
>>* Started moving services to k8s
>>* Implement IaaC - first on Terraform, moving to Pulumi
>>* Implement HA monitoring based on VictoriaMetrics
>>* Implement HA secrets storage based on Hashicorp Vault
>>* Increase products availability
>
>### June 2017 - Mar 2021: SRE
>
>>_Customer:_  Internal company services\
>>_Team Size:_ 1-10\
>>_Project Role:_ SRE\
>>_Employment type:_ Part-time\
>>_Daily routine:_
>>
>>* Automate company and external customers projects
>>* Working with external auditors to implement better security
>>* Ensure CI/CD ptocess for all company projects
>>* Work on monitoring improvements
>>
>>_Tasks Performed:_
>>
>>* Created CI/CD system based on TravisCI/AWS CodePipeline
>>* Created automated monitoring and support of internal infrastructure services – hundreds of metrics per minute
>>* Fully automated infrastructure changes deployment based on Ansible stack
>>* Maintain deployment tools as simple as possible, help developers to learn Ops stuff
>>* Implemented PCI DSS requirements to pass external audit
>>* Implemented centralized logging solution from scratch
>>* Implemented centralized backup solution from scratch


### **GoDaddy**, <https://www.godaddy.com>
>### June 2017 - Mar 2021: SRE
>_Customer:_  Internal company services\
>_Team Size:_ 10-20\
>_Project Role:_ Senior Reliability Engineer\
>_Employment type:_ Full-time\
>_Daily routine:_
>
>* Create and improve internal cloud services
>* Maintain existing internal cloud
>* Identify, assist in planning and implement solutions that continually
>  improves performance
>* Identify, assist in creating and maintain automation which allows for
>  self-healing and incident resolution
>* On-call rotations and Incident call handling
>* Work across multiple distributed teams to>accomplish internal and external
>  team integrations
>
>_Tasks Performed:_
>
>* Developed internal servers capacity extraction tool
>* Developed internal capacity exhaustion prediction tool
>* Improved ElasticSearch Kibana Dashboards to look at servers capacity and trends
>* Created automated key rotation mechanism for Vault-encrypted files
>* Created automation jobs to extract clusters info from OpenStack API
>* Upgraded existing Kolla repos to upstream while saved downstream work
>* Gradually onboard several new team members from zero to hero
>* Developed automatic tooling to issue TLS keypairs via different sources
>* Planned new log ingestion tooling and implement part of it
>* Moved internal tools from Python 2 to Python 3 codebase


###  **Billionapp**
>### June 2017 - November 2018: DevOps
>_Customer:_  Internal company services\
>_Team Size:_ 1-10\
>_Project Role:_ DevOps\
>_Employment type:_ Part-time\
>_Tasks Performed:_
>
>* Created CI/CD system based on Jenkins from scratch
>* Created semi-automated monitoring and support of internal infrastructure services – Jira, BitBucket, Confluence, Mail services
>* Dramatically increased time to deliver company services to production – from weeks to hours
>* Fully automated infrastructure changes deployment based on Ansible stack
>* Moved all company services from Docker Swarm to Kubernetes


### **Mirantis**, <https://www.mirantis.com>
>### June 2014 - May 2017: SDE
>_Customer:_  As a key deployment engineer worked for all company customers cloud-related projects\
>_Project:_ Fuel – the biggest and most powerful official OpenStack deployment tool\
>_Team Size:_ 50-200\
>_Project Role:_ Senior Deployment Engineer\
>_Employment type:_ Full-time\
>_Tasks performed:_
>
>* Developed Puppet manifests to deploy maintenance updates of Mirantis OpenStack cloud clusters
>* Maintain deployment manifests of already shipped releases of Mirantis OpenStack
>* Developed and backported a lot of patches for Mirantis OpenStack cloud components
>* Developed full-fledged cloud product TLS security solution
>* Developed fast and robust time sync cross-cluster solution
>* Participate in developing internal cloud solution for fast deployment (speed up deployment time 5-10 times)
>* As a core reviewer in Fuel Library project done product code reviews (1st product 2017 reviewer and 1st 2016/2017 Fuel-library core component reviewer by number of reviews done)
>* Tools: Puppet, Python, Ruby, Git, OpenStack, S3, VMWare


### **PAO Armada (former RBC-Soft)**, <http://pao-armada.ru/>
>### 2013 - June 2014: Principal Engineer
>_Project:_ GIS for planning, analyze and store >government defense orders.\
>_Team Size:_ 20-200\
>_Project Role:_ Operational TeamLead\
>_Employment type:_ Full-time\
>_Tasks performed:_
>
>* Developed and integrated distributed software system for customer needs
>* Developed technical documentation according to government standard
>* Created internal IT support subsidiary for project needs


### **M-Reason**, <http://m-reason.com>
>### 2011 - 2013: Engineer
>_Project:_ Internal support\
>_Team Size:_ 5\
>_Project Role:_ L2 support/System administrator\
>_Employment type:_ Full-time\
>_Tasks performed:_
>
>* Restructured network solutions, created external VPN for all subsidiaries
>* Created SSO and automated external software integration into it


### **JSC Sigma**
>### 2006 - 2011: Engineer
>_Project:_ Internal support\
>_Team Size:_ 4\
>_Project Role:_ L2 support/System administrator\
>_Tasks performed:_
>
>* Unifed network scheme across all company branches
>* Develop and integrate corporate mail/antivirus/antispam solutions
>* Created and supported mobile trading system for internal company needs

---

## Education

_Graduation Year:_ 2011\
_Name of the Education Establishment:_ Tambov State Technical University\
_Faculty/College:_ Informational systems\
_Degree (diploma):_ Master of Science (officially evaluated by Trustforce Corp.)\
_Specialty:_ Informational systems and technologies


## Certifications:

_Cisco:_ CCNA\
_TOEIC:_ 860

## Contact Information:

_mailto:_ <stabog.tmb@gmail.com>\
_skype:_ sur1kan0\
_phone/Telegram:_ +79264934575
